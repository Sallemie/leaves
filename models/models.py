# -*- coding: utf-8 -*-
from odoo import exceptions, _
from datetime import datetime
from odoo import models, fields, api


class leaves(models.Model):
     _name = 'leaves.leaves'
     _inherit = ['mail.thread', 'mail.activity.mixin']

     
    
     def _get_employee_id(self):
        employee_rec = self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)
        return employee_rec.id


     type = fields.Selection([('Sick leave', 'Sick leave'), ('Annual leave', 'Annual leave'),('unpaid','Unpaid leave')] )
     start_date = fields.Date()
     end_date = fields.Date()
     duration = fields.Integer(compute="_compute_duration",required=True)
     employee_ids = fields.Many2one('hr.employee', 'Employee',ondelete='cascade',default=_get_employee_id, readonly=True, required=True) 
     description = fields.Text()
     state=fields.Selection([
       ('draft','To Submit'),
       ('submitted','To Approve'),
       ('validate','Manager Approval'),
       ('validate1','HR Approval'),
       ('reject','Rejected'),
       ('approved','Approved'),
       ('cancel','Canceled')
     ],string="Status",readonly=True,default='draft')


     
     @api.one
     @api.depends('end_date')
     def _compute_duration(self):  
           for rec in self:
             if rec.start_date and rec.end_date:
                 s_date=datetime.strptime(str(rec.start_date), "%Y-%m-%d").date()
                 e_date=datetime.strptime(str(rec.end_date), "%Y-%m-%d").date() 
                 result=(e_date-s_date).days
                 if result>0:
                     if result<= self.employee_ids.leaves_balance:
                       self.duration= result
                     else:
                       raise exceptions.except_orm(_('Warnning'), _('You do not have enough days, Your Leave Balance: '+str(self.employee_ids.leaves_balance)))
                 else:
                   raise exceptions.except_orm(_('Warnning'), _('Please enter valid dates'))
                         
 
        



                                             ##########################################
                                             # *********** Leaves Actions *********** #
                                             ##########################################



   # ** Submit Leave Action **

     def action_submit(self):
       for rec in self:
         rec.state='submitted'
         temp = rec.employee_ids.leaves_balance - rec.duration
         rec.env['hr.employee'].search([('name', '=', rec.employee_ids.name)]).write({'leaves_balance': temp}) 
        #  rec.employee_ids.parent_id.user_id.notify_info("You have Leave request")
         template_id = self.env.ref('leaves.leaves_email_template').id
         rec.env['mail.template'].browse(template_id).send_mail(self.id,force_send=True)
         self.message_post(body="your message", partner_ids=rec.employee_ids.parent_id.user_id)
         
        



  # ** Cancel Leave Action ** 
     def action_cancel(self):
        for rec in self:
          if rec.state!='validate' and rec.state!='validate1':
            rec.state='cancel' 
            temp = rec.employee_ids.leaves_balance +rec.duration
            rec.env['hr.employee'].search([('name', '=', rec.employee_ids.name)]).write({'leaves_balance': temp}) 
          else:
             raise exceptions.except_orm(_('Erorr'), _('Your can not cancle it! (Your manager approved it)'))





  # ** Reject Leave Action **
     def action_reject(self):
       for rec in self:
         rec.state='reject'
         temp = rec.employee_ids.leaves_balance + rec.duration
         rec.env['hr.employee'].search([('name', '=', rec.employee_ids.name)]).write({'leaves_balance': temp}) 





   # ** Manager Approval Action **

     def action_validate(self):
        for rec in self:
          if rec.type == 'unpaid':
            return self.write({'state': 'validate1'})
          else:
            return self.write({'state': 'approved'})
        




   # ** HR Approval Action **

     def action_validate1(self):
       for rec in self:
         rec.state='approved'
